﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using MazeMouse.MazeElements;
using MazeMouse.MazeElements.elements;

namespace MazeMouse
{
    public sealed partial class MainForm : Form
    {
        private int SizePixel;
            
        private Maze _maze;
        private Mouse _mouse;
        private Point _startSolution = new(30, 50);
        private Point _startDefi = new(30, 50);

        public MainForm()
        {
            InitializeComponent();
            DoubleBuffered = true;
            gbxDefiMaze.Paint += gbxDefiMaze_Paint;
            SizePixel = gbxDefiMaze.Size.Width;

            LoadMaze(@"D:\Users\Tom\Documents\Developpement Local\Repos\Git\cfpt-mazemouse\MazeMouse\50-debug.txt");
        }

        private void LoadMaze(string path)
        {
            _maze = MazeController.Parse(path);

            _mouse = new Mouse(_maze, SizePixel, gbxDefiMaze);
            _mouse.StartPixel = new Point((int)Math.Round(SizePixel / _maze.MazeSize.Width / 1.5), SizePixel / _maze.MazeSize.Height);

            Bitmap bitmap = new Bitmap(SizePixel + _startDefi.X, SizePixel * _maze.MazeSize.Height / _maze.MazeSize.Width + _startDefi.Y);
            Bitmap bitmap2 = new Bitmap(SizePixel + _startDefi.X, SizePixel * _maze.MazeSize.Height / _maze.MazeSize.Width + _startDefi.Y);
            
            Graphics g = Graphics.FromImage(bitmap);
            Graphics g2 = Graphics.FromImage(bitmap2);
            
            MazeController.PaintMouseMaze(g, _maze, SizePixel, _startDefi);
            MazeController.PaintSolutionMaze(g2, _maze, SizePixel, _startSolution);

            gbxDefiMaze.BackgroundImage = bitmap;
            gbxSolMaze.BackgroundImage = bitmap2;
        }

        private void gbxDefiMaze_Paint(object sender, PaintEventArgs e)
        {
            _mouse?.PrintMouse(e.Graphics);
        }

        private void btns_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            switch (btn.Name)
            {
                case "btnOpen":
                    if (ofdMazeFile.ShowDialog() == DialogResult.OK)
                        LoadMaze(ofdMazeFile.FileName);
                    break;
                case "btnStartStop":
                    if (btnStartStop.Text == "Lancer")
                    {
                        btnStartStop.Text = "Arreter";
                        _mouse.Start();
                    }
                    else
                    {
                        btnStartStop.Text = "Lancer";
                        _mouse.Stop();
                    }

                    break;
                case "btnStepByStep":
                    _mouse.Move();
                    break;
            }
        }
    }
}