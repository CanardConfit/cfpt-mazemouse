﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MazeMouse.MazeElements.elements
{
    public class CellNode : ICloneable
    {
        [AllowNull]
        public CellNode Children { get; set; }
        
        [AllowNull]
        public CellNode Parent { get; set; }
        
        public Cell Current { get; }
        
        public int Index { get; }

        public CellNode(CellNode cellParent, CellNode cellChildren, Cell current)
        {
            Parent = cellParent;
            Children = cellChildren;
            Current = current;
        }

        public object Clone()
        {
            return new CellNode(this, Children?.Clone() as CellNode, Current);
        }
    }
}