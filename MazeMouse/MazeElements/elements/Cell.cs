﻿using System;

namespace MazeMouse.MazeElements.elements
{
    public class Cell
    {
        public int Y { get; }
        public int X { get; }
        public byte Walls { get; }

        public Cell(int y, int x, byte direction)
        {
            Y = y;
            X = x;
            Walls = direction;
        }

        public override string ToString()
        {
            return $"X:{X} Y:{Y} Walls:{Walls:X}";
        }
    }
}