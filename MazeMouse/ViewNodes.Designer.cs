﻿using System.ComponentModel;

namespace MazeMouse
{
    partial class ViewNodes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbxDetail = new System.Windows.Forms.ListBox();
            this.lbxNode = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbxDetail
            // 
            this.lbxDetail.FormattingEnabled = true;
            this.lbxDetail.ItemHeight = 16;
            this.lbxDetail.Location = new System.Drawing.Point(12, 12);
            this.lbxDetail.Name = "lbxDetail";
            this.lbxDetail.Size = new System.Drawing.Size(324, 420);
            this.lbxDetail.TabIndex = 0;
            // 
            // lbxNode
            // 
            this.lbxNode.FormattingEnabled = true;
            this.lbxNode.ItemHeight = 16;
            this.lbxNode.Location = new System.Drawing.Point(464, 12);
            this.lbxNode.Name = "lbxNode";
            this.lbxNode.Size = new System.Drawing.Size(324, 420);
            this.lbxNode.TabIndex = 1;
            this.lbxNode.SelectedValueChanged += new System.EventHandler(this.lbxNode_SelectedValueChanged);
            // 
            // ViewNodes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbxNode);
            this.Controls.Add(this.lbxDetail);
            this.Name = "ViewNodes";
            this.Text = "ViewNodes";
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.ListBox lbxDetail;
        private System.Windows.Forms.ListBox lbxNode;

        #endregion
    }
}