﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using MazeMouse.MazeElements.elements;

namespace MazeMouse.MazeElements
{
    public static class MazeController
    {
        private const char DirectionWallUpdown = '-';
        private const char DirectionWallLeftright = '|';

        public const char TypeGoal = 'G';
        public const char TypeStart = 'S';
        
        public const byte Empty = 0x00;
        public const byte North = 0x01;
        public const byte East = 0x02;
        public const byte South = 0x04;
        public const byte West = 0x08;

        public static Maze Parse(string filename)
        {
            string[] lines = File.ReadAllLines(filename);
            
            Size mazeSize = new Size(lines[0].Length / 4, lines.Length / 2);

            Maze maze = new Maze(mazeSize);

            List<Cell> goalCells = new();
            
            for (int y = 0; y < lines.Length - 2; y += 2)
            {
                for (int x = 0; x < lines[0].Length - 4; x += 4)
                {
                    char n = lines[  y  ][x + 2];
                    char e = lines[y + 1][x + 4];
                    char s = lines[y + 2][x + 2];
                    char w = lines[y + 1][  x  ];

                    char type = lines[y + 1][x + 2];
                    
                    byte walls = Empty;

                    walls += n == DirectionWallUpdown    ? North : Empty;
                    walls += e == DirectionWallLeftright ? East  : Empty;
                    walls += s == DirectionWallUpdown    ? South : Empty;
                    walls += w == DirectionWallLeftright ? West  : Empty;

                    int realX = x / 4;
                    int realY = y / 2;
                    
                    Cell cell = new Cell(realY, realX, walls);
                    maze.setCell(realY, realX, cell);
                    
                    if (type == TypeGoal) goalCells.Add(cell);
                    if (type == TypeStart) maze.CellStart = cell;
                }
            }

            maze.CellGoal = goalCells.ToArray();
            
            return maze;
        }

        public static void PaintMouseMaze(Graphics g, Maze maze, int size, Point start)
        {
            int splitSizeX = size / maze.MazeSize.Width;
            int splitSizeY = size * maze.MazeSize.Width / maze.MazeSize.Height / maze.MazeSize.Height;

            int offsetX = splitSizeX / 2;
            int offsetY = splitSizeY / 2;
            
            Pen pen = new Pen(maze.ColorMain);
            
            foreach (Cell cell in maze.Cells)
            {
                int x = cell.X * splitSizeX + start.X;
                int y = cell.Y * splitSizeY + start.Y;

                byte dir = cell.Walls;
                
                // Debug direction
                g.DrawString(cell.X + ";" + cell.Y, new Font("Times New Roman", 10.0f), new SolidBrush(Color.Black), new PointF(x-20, y-20));
                g.DrawString(dir.ToString(), new Font("Times New Roman", 10.0f), new SolidBrush(Color.Black), new PointF(x, y));

                Point startRect = new Point(x - offsetX, y - offsetY);
                Size stopRect = new Size(offsetX * 2, offsetY * 2);
                
                if (maze.isGoalSell(cell))
                    g.FillRectangle(new SolidBrush(maze.ColorEnd), new Rectangle(startRect, stopRect));
                else if (maze.isStartSell(cell))
                    g.FillRectangle(new SolidBrush(maze.ColorStart), new Rectangle(startRect, stopRect));
                
                if (dir >= West)
                {
                    dir -= West;
                    Point pW1 = new Point(x - offsetX, y - offsetY);
                    Point pW2 = new Point(x - offsetX, y + offsetY);
                
                    g.DrawLine(pen, pW1, pW2);
                }
                if (dir >= South)
                {
                    dir -= South;
                    Point pS1 = new Point(x - offsetX, y + offsetY);
                    Point pS2 = new Point(x + offsetX, y + offsetY);
                    
                    g.DrawLine(pen, pS1,pS2);
                }
                if (dir >= East)
                {
                    dir -= East;
                    Point pE1 = new Point(x + offsetX, y - offsetY);
                    Point pE2 = new Point(x + offsetX, y + offsetY);
                    
                    g.DrawLine(pen, pE1, pE2);
                }
                if (dir >= North)
                {
                    Point pN1 = new Point(x - offsetX, y - offsetY);
                    Point pN2 = new Point(x + offsetX, y - offsetY);
                    
                    g.DrawLine(pen, pN1, pN2);
                }
            }
        }
        public static void PaintSolutionMaze(Graphics g, Maze maze, int size, Point start)
        {
            int splitSizeX = size / maze.MazeSize.Width;
            int splitSizeY = size * maze.MazeSize.Width / maze.MazeSize.Height / maze.MazeSize.Height;

            int offsetX = splitSizeX / 2;
            int offsetY = splitSizeY / 2;
            
            Pen pen = new Pen(maze.ColorMain);
            
            foreach (Cell cell in maze.Cells)
            {
                int x = cell.X * splitSizeX + start.X;
                int y = cell.Y * splitSizeY + start.Y;

                byte dir = cell.Walls;
                
                // Debug direction
                //g.DrawString(dir.ToString(), Font, new SolidBrush(Color.Black), new PointF(x, y));

                Point startRect = new Point(x - offsetX, y - offsetY);
                Size stopRect = new Size(offsetX * 2, offsetY * 2);
                
                if (maze.isGoalSell(cell))
                    g.FillRectangle(new SolidBrush(maze.ColorEnd), new Rectangle(startRect, stopRect));
                else if (maze.isStartSell(cell))
                    g.FillRectangle(new SolidBrush(maze.ColorStart), new Rectangle(startRect, stopRect));
                
                if (dir >= West)
                {
                    dir -= West;
                    Point pW1 = new Point(x - offsetX, y - offsetY);
                    Point pW2 = new Point(x - offsetX, y + offsetY);
                
                    g.DrawLine(pen, pW1, pW2);
                }
                if (dir >= South)
                {
                    dir -= South;
                    Point pS1 = new Point(x - offsetX, y + offsetY);
                    Point pS2 = new Point(x + offsetX, y + offsetY);
                    
                    g.DrawLine(pen, pS1,pS2);
                }
                if (dir >= East)
                {
                    dir -= East;
                    Point pE1 = new Point(x + offsetX, y - offsetY);
                    Point pE2 = new Point(x + offsetX, y + offsetY);
                    
                    g.DrawLine(pen, pE1, pE2);
                }
                if (dir >= North)
                {
                    Point pN1 = new Point(x - offsetX, y - offsetY);
                    Point pN2 = new Point(x + offsetX, y - offsetY);
                    
                    g.DrawLine(pen, pN1, pN2);
                }
            }
        }
    }
}