﻿using System.ComponentModel;

namespace MazeMouse
{
    sealed partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ofdMazeFile = new System.Windows.Forms.OpenFileDialog();
            this.gbxRegMaze = new System.Windows.Forms.GroupBox();
            this.btnStartStop = new System.Windows.Forms.Button();
            this.btnStepByStep = new System.Windows.Forms.Button();
            this.btnOpen = new System.Windows.Forms.Button();
            this.gbxDefiMaze = new System.Windows.Forms.GroupBox();
            this.gbxSolMaze = new System.Windows.Forms.GroupBox();
            this.gbxRegMaze.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofdMazeFile
            // 
            this.ofdMazeFile.Filter = "Maze File|*.txt|All files|*.*";
            this.ofdMazeFile.InitialDirectory = "C:\\Users\\TOM.ANDRV\\Documents\\Repos\\mazefiles\\classic";
            this.ofdMazeFile.Title = "Choisir un fichier contenant un labyrithe";
            // 
            // gbxRegMaze
            // 
            this.gbxRegMaze.Controls.Add(this.btnStartStop);
            this.gbxRegMaze.Controls.Add(this.btnStepByStep);
            this.gbxRegMaze.Controls.Add(this.btnOpen);
            this.gbxRegMaze.Location = new System.Drawing.Point(16, 15);
            this.gbxRegMaze.Margin = new System.Windows.Forms.Padding(4);
            this.gbxRegMaze.Name = "gbxRegMaze";
            this.gbxRegMaze.Padding = new System.Windows.Forms.Padding(4);
            this.gbxRegMaze.Size = new System.Drawing.Size(1560, 123);
            this.gbxRegMaze.TabIndex = 0;
            this.gbxRegMaze.TabStop = false;
            this.gbxRegMaze.Text = "Reglage du Labyrinthe";
            // 
            // btnStartStop
            // 
            this.btnStartStop.Location = new System.Drawing.Point(1227, 23);
            this.btnStartStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStartStop.Name = "btnStartStop";
            this.btnStartStop.Size = new System.Drawing.Size(149, 92);
            this.btnStartStop.TabIndex = 2;
            this.btnStartStop.Text = "Lancer";
            this.btnStartStop.UseVisualStyleBackColor = true;
            this.btnStartStop.Click += new System.EventHandler(this.btns_Click);
            // 
            // btnStepByStep
            // 
            this.btnStepByStep.Location = new System.Drawing.Point(1384, 23);
            this.btnStepByStep.Margin = new System.Windows.Forms.Padding(4);
            this.btnStepByStep.Name = "btnStepByStep";
            this.btnStepByStep.Size = new System.Drawing.Size(168, 92);
            this.btnStepByStep.TabIndex = 1;
            this.btnStepByStep.Text = "Step";
            this.btnStepByStep.UseVisualStyleBackColor = true;
            this.btnStepByStep.Click += new System.EventHandler(this.btns_Click);
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(8, 23);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(181, 92);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Ouvrir...";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btns_Click);
            // 
            // gbxDefiMaze
            // 
            this.gbxDefiMaze.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbxDefiMaze.Location = new System.Drawing.Point(16, 145);
            this.gbxDefiMaze.Margin = new System.Windows.Forms.Padding(4);
            this.gbxDefiMaze.Name = "gbxDefiMaze";
            this.gbxDefiMaze.Padding = new System.Windows.Forms.Padding(4);
            this.gbxDefiMaze.Size = new System.Drawing.Size(767, 782);
            this.gbxDefiMaze.TabIndex = 1;
            this.gbxDefiMaze.TabStop = false;
            this.gbxDefiMaze.Text = "Défi";
            // 
            // gbxSolMaze
            // 
            this.gbxSolMaze.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gbxSolMaze.Location = new System.Drawing.Point(809, 145);
            this.gbxSolMaze.Margin = new System.Windows.Forms.Padding(4);
            this.gbxSolMaze.Name = "gbxSolMaze";
            this.gbxSolMaze.Padding = new System.Windows.Forms.Padding(4);
            this.gbxSolMaze.Size = new System.Drawing.Size(767, 782);
            this.gbxSolMaze.TabIndex = 1;
            this.gbxSolMaze.TabStop = false;
            this.gbxSolMaze.Text = "Solution du Labyrinthe";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1592, 942);
            this.Controls.Add(this.gbxSolMaze);
            this.Controls.Add(this.gbxDefiMaze);
            this.Controls.Add(this.gbxRegMaze);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "MazeMouse";
            this.gbxRegMaze.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnStepByStep;
        private System.Windows.Forms.Button btnStartStop;

        private System.Windows.Forms.GroupBox gbxRegMaze;
        private System.Windows.Forms.GroupBox gbxDefiMaze;
        private System.Windows.Forms.GroupBox gbxSolMaze;

        private System.Windows.Forms.OpenFileDialog ofdMazeFile;

        #endregion
    }
}