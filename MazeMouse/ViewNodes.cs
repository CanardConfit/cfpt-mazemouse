﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MazeMouse.MazeElements.elements;

namespace MazeMouse
{
    public partial class ViewNodes : Form
    {
        private List<CellNode> _nodes;

        private ViewNodes(List<CellNode> nodes)
        {
            _nodes = nodes;
            InitializeComponent();
            foreach (CellNode node in nodes)
            {
                lbxNode.Items.Add($"Node #{nodes.IndexOf(node)} | {node.Index}");
            }
        }

        public static DialogResult Show(List<CellNode> nodes)
        {
            ViewNodes form = new ViewNodes(nodes);
            form.ShowDialog();
            return DialogResult.OK;
        }

        private void lbxNode_SelectedValueChanged(object sender, EventArgs e)
        {
            lbxDetail.Items.Clear();
            CellNode node = _nodes[lbxNode.SelectedIndex];
            do
            {
                lbxDetail.Items.Add(node.Current.ToString());
            } while ((node = node.Parent) != null);
        }
    }
}