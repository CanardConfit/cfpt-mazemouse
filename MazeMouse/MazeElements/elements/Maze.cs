﻿using System.Drawing;

namespace MazeMouse.MazeElements.elements
{
    public class Maze
    {

        public Color ColorMain { get; set; }
        public Color ColorStart { get; set; }
        public Color ColorEnd { get; set; }
        public Cell[,] Cells { get; }
        
        public Cell CellStart { get; set; }
        
        public Cell[] CellGoal { get; set; }
        
        public Size MazeSize { get; }                                                                                            
        public Size CellSize { get; }                                                                                            

        public Maze(Size mazeSize, Cell[,] cells, Size cellSize)
        {
            MazeSize   = mazeSize;
            Cells      = cells;
            CellSize   = cellSize;
            ColorMain  = Color.Black;
            ColorStart = Color.FromArgb(40, Color.Green);
            ColorEnd   = Color.FromArgb(40, Color.Red);
        }

        public Maze(Size mazeSize, Size cellSize) : this(mazeSize, new Cell[mazeSize.Height, mazeSize.Width], cellSize) {}
        public Maze(Size mazeSize) : this(mazeSize,  new Size(50, 50)) {}

        public void setCell(int y, int x, Cell cell)
        {
            Cells[y,x] = cell;
        }
        public bool isGoalSell(int y, int x)
        {
            return Contains(CellGoal, Cells[y,x]);
        }
        public bool isGoalSell(Cell cell)
        {
            return Contains(CellGoal, cell);
        }
        public bool isStartSell(int y, int x)
        {
            return CellStart == Cells[y,x];
        }
        public bool isStartSell(Cell cell)
        {
            return CellStart == cell;
        }

        private static bool Contains<T>(T[] list, T element)
        {
            foreach (T el in list)
                if (el.Equals(element))
                    return true;
            return false;
        }
    }
}