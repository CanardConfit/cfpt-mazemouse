﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace MazeMouse.MazeElements.elements
{
    public class Mouse
    {
        private readonly Maze _maze;

        private readonly int _multiplierX;
        private readonly int _multiplierY;
        private readonly int _sizePixel;
        private readonly Random _rnd;
        
        private Timer _timer;
        
        public int X { get; set; }
        public int Y { get; set; }
        
        public Direction Direction { get; set; }
        
        public Point StartPixel { get; set; }
        
        private List<CellNode> _nodesFinish = new();
        private List<CellNode> _nodesRunning = new();
        private CellNode _currentNode;
        private Control _control;

        public Mouse(Maze maze, int x, int y, int sizePixel, Control control) : this(maze, sizePixel, control)
        {
            X = x;
            Y = y;
        }

        public Mouse(Cell depart, Maze maze, int sizePixel, Control control) : this(maze, sizePixel, control)
        {
            X = depart.X;
            Y = depart.Y;
        }

        public Mouse(Maze maze, int sizePixel, Control control)
        {
            _rnd = new Random();
            _maze = maze;
            _currentNode = new CellNode(null, null, _maze.CellStart);
            _nodesRunning.Add(_currentNode);
            
            _timer = new Timer(100);
            _timer.Elapsed += Tick;
            _timer.Enabled = false;
            _timer.AutoReset = true;
            X = _maze.CellStart.X;
            Y = _maze.CellStart.Y;
            _sizePixel = sizePixel;
            _control = control;
            _multiplierX = _sizePixel / maze.MazeSize.Width;
            _multiplierY = _sizePixel * maze.MazeSize.Width / maze.MazeSize.Height / maze.MazeSize.Height;
        }

        private void Tick(object sender, ElapsedEventArgs e)
        {
            Move();
        }

        public void Move()
        {
            switch (Direction)
            {
                case Direction.Up:
                    Y -= 1;
                    break;
                case Direction.Down:
                    Y += 1;
                    break;
                case Direction.Left:
                    X += 1;
                    break;
                case Direction.Right:
                    X -= 1;
                    break;
            }

            _control.Invalidate();
            
            Console.WriteLine($"Direction : {Enum.GetName(Direction)}");
            Console.WriteLine($"Finish : {_nodesFinish.Count}");
            Console.WriteLine($"Running : {_nodesRunning.Count}");
            Console.WriteLine("----");

            if (_nodesRunning.Count == 0)
            {
                _timer.Stop();
                ViewNodes.Show(_nodesFinish);
                return;
            }
            
            Cell possibilite = DetectCell();

            bool addCell = true;
            bool resetNode = false;

            switch (possibilite.Walls)
            {
                case 0x0E: // West,South,East
                    if (_currentNode != null && Direction == Direction.Down)
                    {
                        resetNode = true;
                    }
                    Direction = Direction.Up;
                    break;
                case 0x0D: // 13 West,South,North
                    if (_currentNode != null && Direction == Direction.Right)
                    {
                        resetNode = true;
                    }
                    Direction = Direction.Left;
                    break;
                case 0x0C: // 12 West,South
                    Direction = Direction == Direction.Right ? Direction.Up : Direction.Left;
                    break;
                case 0x0B: // West,East,North
                    if (_currentNode != null && Direction == Direction.Up)
                    {
                        resetNode = true;
                    }
                    Direction = Direction.Down;
                    break;
                case 0x0A: // 10 West,East
                    Direction = Direction == Direction.Down ? Direction.Down : Direction.Up;
                    break;
                case 0x09: // 9 West,North
                    Direction = Direction == Direction.Right ? Direction.Down : Direction.Left;
                    break;
                case 0x08: // 8 West
                    break;
                case 0x07: // 7 South,East,North
                    if (_currentNode != null && Direction == Direction.Left)
                    {
                        resetNode = true;
                    }
                    Direction = Direction.Right;
                    break;
                case 0x06: // 6 South,East
                    Direction = Direction == Direction.Left ? Direction.Up : Direction.Right;
                    break;
                case 0x05: // 5 South,North
                    Direction = Direction == Direction.Left ? Direction.Left : Direction.Right;
                    break;
                case 0x04: // 4 South
                    break;
                case 0x03: // 3 East,North
                    Direction = Direction == Direction.Left ? Direction.Down : Direction.Right;
                    break;
                case 0x02: // 2 East
                    if (_currentNode != null)
                    {
                        CellNode clone = (CellNode) _currentNode.Clone();
                        _nodesRunning.Add(clone);
                        
                        Direction =
                            Direction == Direction.Left ?
                            new[]
                            {
                                Direction.Up, Direction.Down
                            }[_rnd.Next(0, 2)] :
                            Direction == Direction.Down ?
                            new[]
                            {
                                Direction.Up, Direction.Right
                            }[_rnd.Next(0, 2)] :
                            new[]
                            {
                                Direction.Right, Direction.Up
                            }[_rnd.Next(0, 2)];
                    }
                    else
                    {
                        foreach (CellNode node in _nodesRunning)
                        {
                            if (NodeUtils.AsCellIn(node, NodeUtils.SearchMethod.Parent, possibilite))
                            {
                                _currentNode = node;
                                break;
                            }
                        }

                        if (_currentNode == null)
                        {
                            Console.WriteLine("Aucun chemin trouvé");
                            Direction =
                                Direction == Direction.Left ?
                                    new[]
                                    {
                                        Direction.Up, Direction.Down
                                    }[_rnd.Next(0, 2)] :
                                    Direction == Direction.Down ?
                                        new[]
                                        {
                                            Direction.Up, Direction.Right
                                        }[_rnd.Next(0, 2)] :
                                        new[]
                                        {
                                            Direction.Right, Direction.Up
                                        }[_rnd.Next(0, 2)];
                        }
                        else
                        {
                            Console.WriteLine("Chemin trouvé");
                            Console.WriteLine($"Chemin X:{_currentNode.Parent.Parent.Current.X}");
                            Console.WriteLine($"Chemin Y:{_currentNode.Parent.Parent.Current.Y}");
                            Direction dirOldCur =
                                _currentNode.Parent.Parent.Current.X > possibilite.X ? Direction.Left :
                                _currentNode.Parent.Parent.Current.Y > possibilite.Y ? Direction.Down :
                                _currentNode.Parent.Parent.Current.X < possibilite.X ? Direction.Right : Direction.Up;

                            Console.WriteLine($"Direction du chemin old : {Enum.GetName(dirOldCur)}");
                            
                            Direction =
                                Direction == Direction.Down && dirOldCur == Direction.Right ?  Direction.Down  :
                                Direction == Direction.Down && dirOldCur == Direction.Down  ?  Direction.Right :
                                Direction == Direction.Left && dirOldCur == Direction.Up    ?  Direction.Down  :
                                Direction == Direction.Left && dirOldCur == Direction.Down  ?  Direction.Up    :
                                Direction == Direction.Up   && dirOldCur == Direction.Up    ?  Direction.Right :
                              /*Direction == Direction.Up   && dirOldCur == Direction.Right ?*/Direction.Up    ;

                            addCell = false;
                        }
                    }

                    break;
                case 0x01: // 1 North
                    break;
            }
            
            if (_currentNode != null && addCell)
            {
                _currentNode.Children = new CellNode(_currentNode, null, possibilite);
                _nodesRunning.Remove(_currentNode);
                _currentNode = _currentNode.Children;
                _nodesRunning.Add(_currentNode);
            }

            if (resetNode)
                ResetCurrentNode();
        }

        private void ResetCurrentNode()
        {
            _nodesFinish.Add(_currentNode);
            _nodesRunning.Remove(_currentNode);
            _currentNode = null;
        }

        private Cell DetectCell()
        {
            Cell cur = _maze.Cells[Y, X];
            return cur;
        }

        public void PrintMouse(Graphics g)
        {
            Rectangle rec = new Rectangle(X * _multiplierX + StartPixel.X - 10, Y * _multiplierY + StartPixel.Y - 10,
                20, 20);
            g.FillRectangle(Brushes.Blue, rec);
        }

        public void Start()
        {
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }
    }
}