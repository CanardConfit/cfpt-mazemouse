﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MazeMouse.MazeElements.elements
{
    public static class NodeUtils
    {
        public enum SearchMethod
        {
            Children,
            Parent
        }
        
        public static CellNode GetFirst([NotNull] CellNode node, [NotNull] SearchMethod searchMethod)
        {
            CellNode ret = (CellNode) typeof(CellNode).GetProperty(Enum.GetName(searchMethod)).GetValue(node);
            return ret == null ? node : GetFirst(ret, searchMethod);
        }
        
        public static bool AsCellIn([NotNull] CellNode nodeMain, [NotNull] SearchMethod searchMethod, [NotNull] Cell cell)
        {
            CellNode node = nodeMain;

            do
                if (node.Current == cell)
                    return true;
            while ((node = (CellNode) typeof(CellNode).GetProperty(Enum.GetName(searchMethod)).GetValue(node)) != null);

            return false;
        }
    }
}